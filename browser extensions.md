# Browser Recommendations

Use [Firefox](https://www.mozilla.org/en-US/firefox/new/ ) or equivalent derivative. 


## Extensions-general

These extensions are "generaly safe" for people who may not be 
technically literate. In no particular order:

- [ClearURLs](https://addons.mozilla.org/en-US/firefox/addon/clearurls/)
    - Removes tracking elements from URLs.
- [Ublock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
    - Block ads/content to make the web look nicer.
- [Dark Reader](https://darkreader.org/)
    - For sites that don't have a built-in dark mode. 
- [FaceBook Container](https://addons.mozilla.org/en-US/firefox/addon/facebook-container/)
    - Prevent Facebook from tracking you around the web.
- [Flagfox](https://addons.mozilla.org/en-US/firefox/addon/flagfox/)
    - Displays a country flag depicting the location of the current website's server and provides a multitude of tools such as site safety checks, whois, translation, similar sites, validation, URL shortening, and more...


## Extensions-strict

These extensions either require more work and/or break some sites.
These are _not_ recommended for those who are tech-illiterate.

- [No Script](https://addons.mozilla.org/en-US/firefox/addon/noscript/)
    - The best security you can get in a web browser!
    - ⚠ It seems uBlock Origin (uBO) provides No Script-like features under [advanced settings](https://github.com/gorhill/uBlock/wiki/Advanced-user-features). Testing uBO to see how it compares to `No Script` and if it does enough to replace it.
- [Firefox Multi-Account Containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/)
    - Lets you keep parts of your online life separated into color-coded tabs. Cookies are separated by container, allowing you to use the web with multiple accounts.


## Extensions-optional

- [Progressive Webapps for Firefox](https://addons.mozilla.org/en-US/firefox/addon/pwas-for-firefox/)
    - A tool to install, manage and use Progressive Web Apps (PWAs) in Mozilla Firefox - **Requires a separate application install to work.**
- [Modern for HackerNews](https://addons.mozilla.org/en-US/firefox/addon/modern-for-hacker-news/) 
    - A redesigned web interface for [Hacker News](https://news.ycombinator.com/)
- [Snowflake](https://addons.mozilla.org/en-US/firefox/addon/torproject-snowflake/)
    - A Tor project to help people in censored countries access the Internet without restrictions.